const { InputValidation } = require('ebased/schema/inputValidation');

class CreateCardValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CREATE_CARD',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        id: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
    });
  }
}

module.exports = { CreateCardValidation };
