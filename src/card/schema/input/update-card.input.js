const { InputValidation } = require('ebased/schema/inputValidation');

class UpdateCardValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.UPDATE_CARD',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        id: { type: String, required: true },
        birthDate: { type: String, required: true },
        birthDateHasChanged: { type: Boolean, required: true },
      },
    });
  }
}

module.exports = { UpdateCardValidation };
