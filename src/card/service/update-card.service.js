const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const updateCard = async (commandPayload, card) => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
    Key: {
      id: commandPayload.id,
    },
    UpdateExpression: 'SET card = :card',
    ExpressionAttributeValues: {
      ':card': {
        number: card.number.toString(),
        expirationDate: card.expirationDate,
        cvv: card.cvv.toString(),
        type: card.type,
      },
    },
  };

  await dynamo.updateItem(params);
};

module.exports = { updateCard };
