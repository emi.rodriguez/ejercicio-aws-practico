const { getAgeFromBirthDate } = require('../../client/helper/age.helper');
const { CreateCardValidation } = require('../schema/input/create-card.input');
const { createCard } = require('../service/create-card.service');
const {
  getCreditCardNumber,
  getCreditCardExpirationDate,
  getCreditCardSecurityCode,
  getCreditCardTypeByAge,
} = require('../helper/card.helper');

const createCardDomain = async (eventPayload, commandMeta) => {
  new CreateCardValidation(eventPayload, commandMeta);

  const age = getAgeFromBirthDate(eventPayload.birthDate);

  const card = {
    number: getCreditCardNumber(),
    expirationDate: getCreditCardExpirationDate().toISOString(),
    cvv: getCreditCardSecurityCode(),
    type: getCreditCardTypeByAge(age),
  };

  await createCard(eventPayload, card);

  return {
    status: 200,
    body: 'Card created',
  };
};

module.exports = { createCardDomain };
