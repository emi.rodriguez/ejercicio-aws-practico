const { getAgeFromBirthDate } = require('../../client/helper/age.helper');
const { createCard } = require('../service/create-card.service');
const {
  getCreditCardNumber,
  getCreditCardExpirationDate,
  getCreditCardSecurityCode,
  getCreditCardTypeByAge,
} = require('../helper/card.helper');
const { UpdateCardValidation } = require('../schema/input/update-card.input');
const { updateCard } = require('../service/update-card.service');

const updateCardDomain = async (eventPayload, commandMeta) => {
  new UpdateCardValidation(eventPayload, commandMeta);

  if (!eventPayload.birthDateHasChanged) {
    return {
      status: 200,
      body: 'Card not updated',
    };
  }

  const age = getAgeFromBirthDate(eventPayload.birthDate);

  const card = {
    number: getCreditCardNumber(),
    expirationDate: getCreditCardExpirationDate().toISOString(),
    cvv: getCreditCardSecurityCode(),
    type: getCreditCardTypeByAge(age),
  };

  await updateCard(eventPayload, card);

  return {
    status: 200,
    body: 'Card updated',
  };
};

module.exports = { updateCardDomain };
