const { batchEventMapper } = require('ebased/handler');
const inputMode = require('ebased/handler/input/batchEventQueue');
const outputMode = require('ebased/handler/output/batchEventConfirmation');

const { updateCardDomain } = require('../domain/update-card.domain.js');

module.exports.handler = async (events, context) => {
  return batchEventMapper(
    { events, context },
    inputMode,
    updateCardDomain,
    outputMode
  );
};
