const getCreditCardTypeByAge = (age) => {
  if (age < 45) {
    return 'classic';
  }

  return 'gold';
};

const getCreditCardSecurityCode = () => {
  return Math.floor(Math.random() * 1000);
};

const getCreditCardExpirationDate = () => {
  const today = new Date();
  const expirationDate = new Date();
  expirationDate.setFullYear(today.getFullYear() + 5);
  return expirationDate;
};

const getCreditCardNumber = () => {
  return Math.floor(Math.random() * 1000000000000000);
};

module.exports = {
  getCreditCardTypeByAge,
  getCreditCardSecurityCode,
  getCreditCardExpirationDate,
  getCreditCardNumber,
};
