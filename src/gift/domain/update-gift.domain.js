const {
  getSeasonByBirthDate,
  getGiftBySeason,
} = require('../helper/gift.helper');
const { UpdateGiftValidation } = require('../schema/input/update-gift.input');
const { updateGift } = require('../service/update-gift.servie');

const updateGiftDomain = async (eventPayload, commandMeta) => {
  new UpdateGiftValidation(eventPayload, commandMeta);

  if (!eventPayload.birthDateHasChanged) {
    return {
      status: 200,
      body: 'Gift not updated',
    };
  }

  const season = getSeasonByBirthDate(eventPayload.birthDate);
  const gift = getGiftBySeason(season);

  await updateGift(eventPayload, gift);

  return {
    statusCode: 200,
    body: 'Gift updated',
  };
};

module.exports = { updateGiftDomain };
