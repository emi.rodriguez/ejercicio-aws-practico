const { CreateGiftValidation } = require('../schema/input/create-gift.input');

const { createGift } = require('../service/create-gift.service');

const {
  getSeasonByBirthDate,
  getGiftBySeason,
} = require('../helper/gift.helper');

const createGiftDomain = async (eventPayload, commandMeta) => {
  new CreateGiftValidation(eventPayload, commandMeta);

  const season = getSeasonByBirthDate(eventPayload.birthDate);
  const gift = getGiftBySeason(season);

  await createGift(eventPayload, gift);

  return {
    statusCode: 200,
    body: 'Gift created',
  };
};

module.exports = { createGiftDomain };
