const getSeasonByBirthDate = (birthDate) => {
  const birthDateDate = new Date(birthDate);
  const month = birthDateDate.getMonth();
  const day = birthDateDate.getDay();

  if (month >= 1 && month <= 3) {
    if (month === 3 && day >= 21) {
      return 'fall';
    }
    return 'summer';
  }

  if (month >= 4 && month <= 6) {
    if (month === 6 && day >= 21) {
      return 'winter';
    }
    return 'fall';
  }

  if (month >= 7 && month <= 9) {
    if (month === 9 && day >= 21) {
      return 'spring';
    }
    return 'winter';
  }

  if (month >= 10 && month <= 12) {
    if (month === 12 && day >= 21) {
      return 'summer';
    }
    return 'spring';
  }

  throw new Error('Invalid birth date');
};

const getGiftBySeason = (season) => {
  if (season === 'summer') {
    return 'Remera';
  } else if (season === 'winter') {
    return 'Sweater';
  } else if (season === 'spring') {
    return 'Camisa';
  } else if (season === 'fall') {
    return 'Buzo';
  }

  throw new Error('Invalid season');
};

module.exports = { getSeasonByBirthDate, getGiftBySeason };
