const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const updateGift = async (commandPayload, gift) => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
    Key: {
      id: commandPayload.id,
    },
    UpdateExpression: 'SET gift = :gift',
    ExpressionAttributeValues: {
      ':gift': gift,
    },
  };

  await dynamo.updateItem(params);
};

module.exports = { updateGift };
