const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientUpdated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CLIENT_UPDATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        dni: { type: String, required: true },
        birthDate: { type: String, required: true },
        birthDateHasChanged: { type: Boolean, required: true },
      },
    });
  }
}

module.exports = { ClientUpdated };
