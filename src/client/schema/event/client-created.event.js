const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CLIENT_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        dni: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
    });
  }
}

module.exports = { ClientCreated };
