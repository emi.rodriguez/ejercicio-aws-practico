const { InputValidation } = require('ebased/schema/inputValidation');

class GetClientInputValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.GET',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        id: { type: String, required: true },
      },
    });
  }
}

module.exports = { GetClientInputValidation };
