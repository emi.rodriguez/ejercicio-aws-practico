const getAgeFromBirthDate = (birthDate) => {
  const today = new Date();
  const birthDateDate = new Date(birthDate);
  let age = today.getFullYear() - birthDateDate.getFullYear();
  const month = today.getMonth() - birthDateDate.getMonth();
  if (month < 0 || (month === 0 && today.getDate() < birthDateDate.getDate())) {
    age--;
  }
  return age;
};

module.exports = { getAgeFromBirthDate };
