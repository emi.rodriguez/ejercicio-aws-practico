const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const getAllClients = async () => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
  };

  const { Items } = await dynamo.scanTable(params);

  return Items;
};

module.exports = { getAllClients };
