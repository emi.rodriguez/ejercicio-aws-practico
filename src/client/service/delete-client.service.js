const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const deleteClient = async (commandPayload) => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
    Key: {
      id: commandPayload.id,
    },
    UpdateExpression: 'SET deleted = :deleted',
    ExpressionAttributeValues: {
      ':deleted': true,
    },
  };

  await dynamo.updateItem(params);
};

module.exports = { deleteClient };
