const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const updateClient = async (client) => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
    Key: {
      id: client.id,
    },
    UpdateExpression: 'SET',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: 'ALL_NEW',
  };

  Object.keys(client).forEach((key) => {
    if (key !== 'id') {
      params.UpdateExpression += ` #${key}=:${key},`;
      params.ExpressionAttributeNames[`#${key}`] = key;
      params.ExpressionAttributeValues[`:${key}`] = client[key];
    }
  });
  params.UpdateExpression = params.UpdateExpression.slice(0, -1);
  const { Attributes } = await dynamo.updateItem(params);
  Object.keys(Attributes).forEach((k) => {
    if (k === 'pk' || k === 'sk') delete Attributes[k];
  });

  return Attributes;
};

module.exports = { updateClient };
