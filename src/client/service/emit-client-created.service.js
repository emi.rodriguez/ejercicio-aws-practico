const sns = require('ebased/service/downstream/sns');
const config = require('ebased/util/config');

const emitClientCreatedEvent = async (clientCreatedEvent) => {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();

  await sns.publish(
    {
      TopicArn: config.get('CLIENT_CREATED_TOPIC'),
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { emitClientCreatedEvent };
