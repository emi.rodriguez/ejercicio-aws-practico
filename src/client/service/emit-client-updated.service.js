const sns = require('ebased/service/downstream/sns');
const config = require('ebased/util/config');

const emitClientUpdatedEvent = async (clientUpdatedEvent) => {
  const { eventPayload, eventMeta } = clientUpdatedEvent.get();

  await sns.publish(
    {
      TopicArn: config.get('CLIENT_UPDATED_TOPIC'),
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { emitClientUpdatedEvent };
