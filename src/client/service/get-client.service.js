const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const getClientById = async (id) => {
  const params = {
    TableName: config.get('CLIENT_TABLE'),
    Key: {
      id: id,
    },
  };

  const client = await dynamo.getItem(params);

  return client;
};

const getClientByDni = async (dni) => {
  const { Items } = await dynamo.scanTable({
    TableName: config.get('CLIENT_TABLE'),
    FilterExpression: 'dni = :dni',
    ExpressionAttributeValues: {
      ':dni': dni,
    },
  });

  return Items[0];
};

module.exports = { getClientById, getClientByDni };
