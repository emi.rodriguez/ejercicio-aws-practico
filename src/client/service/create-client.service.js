const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const crypto = require('crypto');

const createClient = async (commandPayload) => {
  const client = {
    id: crypto.randomUUID(),
    ...commandPayload,
  };

  await dynamo.putItem({
    TableName: config.get('CLIENT_TABLE'),
    Item: client,
  });

  return client;
};

module.exports = { createClient };
