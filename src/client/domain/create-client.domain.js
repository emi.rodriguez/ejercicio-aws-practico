const {
  CreateClientValidation,
} = require('../schema/input/create-client.input');

const { getAgeFromBirthDate } = require('../helper/age.helper');

const { createClient } = require('../service/create-client.service');
const {
  emitClientCreatedEvent,
} = require('../service/emit-client-created.service');
const { ClientCreated } = require('../schema/event/client-created.event');

const createClientDomain = async (commandPayload, commandMeta) => {
  new CreateClientValidation(commandPayload, commandMeta);

  const age = getAgeFromBirthDate(commandPayload.birthDate);

  if ((age && age < 18) || age > 65) {
    return {
      status: 400,
      body: 'Client must be between 18 and 65 years old',
    };
  }

  const client = await createClient(commandPayload);
  await emitClientCreatedEvent(new ClientCreated(client, commandMeta));

  return {
    status: 200,
    body: client,
  };
};

module.exports = { createClientDomain };
