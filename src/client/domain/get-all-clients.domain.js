const { getAllClients } = require('../service/get-all-clients.service');

const getAllClientsDomain = async (commandPayload, commandMeta) => {
  const clients = await getAllClients();

  return {
    status: 200,
    body: clients,
  };
};

module.exports = { getAllClientsDomain };
