const { ClientUpdated } = require('../schema/event/client-updated.event');
const {
  UpdateClientValidation,
} = require('../schema/input/update-client.input');
const {
  emitClientUpdatedEvent,
} = require('../service/emit-client-updated.service');
const { getClientById } = require('../service/get-client.service');
const { updateClient } = require('../service/update-client.service');

const updateClientDomain = async (commandPayload, commandMeta) => {
  new UpdateClientValidation(commandPayload, commandMeta);

  let client = await getClientById(commandPayload.id);

  const birthDateHasChanged = client.birthDate !== commandPayload.birthDate;

  client = await updateClient(commandPayload);

  const eventPayload = {
    ...client,
    birthDateHasChanged,
  };

  await emitClientUpdatedEvent(new ClientUpdated(eventPayload, commandMeta));

  return {
    status: 200,
    body: client,
  };
};

module.exports = { updateClientDomain };
