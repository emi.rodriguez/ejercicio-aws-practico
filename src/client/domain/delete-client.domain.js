const {
  DeleteClientInputValidation,
} = require('../schema/input/delete-client.input');
const { deleteClient } = require('../service/delete-client.service');

const deleteClientDomain = async (commandPayload, commandMeta) => {
  new DeleteClientInputValidation(commandPayload, commandMeta);

  const client = await deleteClient(commandPayload);

  return {
    status: 200,
    body: client,
  };
};

module.exports = { deleteClientDomain };
