const {
  GetClientInputValidation,
} = require('../schema/input/get-client.input');
const { getClientById } = require('../service/get-client.service');

const getClientDomain = async (commandPayload, commandMeta) => {
  new GetClientInputValidation(commandPayload, commandMeta);

  const client = await getClientById(commandPayload.id);

  return {
    status: 200,
    body: client,
  };
};

module.exports = { getClientDomain };
