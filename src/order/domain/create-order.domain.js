const { getClientByDni } = require('../../client/service/get-client.service');
const { OrderCreated } = require('../schema/event/order-created.event');
const { CreateOrderValidation } = require('../schema/input/create-order.input');
const { createOrder } = require('../service/create-order.service');
const {
  emitOrderCreatedEvent,
} = require('../service/emit-order-created.service');

const getDiscountByCardType = (cardType) => {
  if (cardType === 'gold') {
    return 0.12;
  }

  return 0.08;
};

const createOrderDomain = async (commandPayload, commandMeta) => {
  new CreateOrderValidation(commandPayload, commandMeta);

  const { dni, products } = commandPayload;

  const client = await getClientByDni(dni);

  if (!client) {
    return {
      status: 400,
      message: 'Client not found',
    };
  }

  if (client.deleted) {
    return {
      status: 400,
      message: 'Client deleted',
    };
  }

  const subtotal = products.reduce((acc, product) => {
    return acc + product.price;
  }, 0);

  const total = subtotal - subtotal * getDiscountByCardType(client.card.type);

  const order = await createOrder({
    dni,
    products,
    subtotal,
    total,
  });

  await emitOrderCreatedEvent(new OrderCreated(order, commandMeta));

  return {
    status: 200,
    body: order,
  };
};

module.exports = { createOrderDomain };
