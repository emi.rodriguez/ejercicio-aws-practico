const {
  CalculateOrderPointsValidation,
} = require('../schema/input/calculate-order-points.input');

const { getClientByDni } = require('../../client/service/get-client.service');
const { updateClient } = require('../../client/service/update-client.service');

const calculateOrderPointsDomain = async (eventPayload, commandMeta) => {
  new CalculateOrderPointsValidation(eventPayload, commandMeta);

  const { subtotal, dni } = eventPayload;

  const client = await getClientByDni(dni);

  let points = client.points ?? 0;
  points += Math.floor(subtotal / 200);

  client.points = points;

  await updateClient(client);

  return {
    status: 200,
    body: 'Points updated',
  };
};

module.exports = { calculateOrderPointsDomain };
