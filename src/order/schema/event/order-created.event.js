const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class OrderCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'ORDER.ORDER_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: String, required: true },
        dni: { type: String, required: true },
        products: { type: Object, required: true },
        subtotal: { type: Number, required: true },
        total: { type: Number, required: true },
      },
    });
  }
}

module.exports = { OrderCreated };
