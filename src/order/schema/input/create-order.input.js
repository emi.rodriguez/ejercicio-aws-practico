const { InputValidation } = require('ebased/schema/inputValidation');

class CreateOrderValidation extends InputValidation {
  constructor(payload, meta) {
    const productSchema = {
      name: { type: String, required: true },
      price: { type: Number, required: true },
    };

    super({
      type: 'ORDER.CREATE',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        dni: { type: String, required: true },
        products: [productSchema],
      },
    });
  }
}

module.exports = { CreateOrderValidation };
