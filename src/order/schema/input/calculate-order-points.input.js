const { InputValidation } = require('ebased/schema/inputValidation');

class CalculateOrderPointsValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'ORDER.CALCULATE_POINTS',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        id: { type: String, required: true },
        subtotal: { type: Number, required: true },
        dni: { type: String, required: true },
      },
    });
  }
}

module.exports = { CalculateOrderPointsValidation };
