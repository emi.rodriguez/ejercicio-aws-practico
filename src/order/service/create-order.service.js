const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const crypto = require('crypto');

const createOrder = async (order) => {
  const item = {
    id: crypto.randomUUID(),
    ...order,
  };

  const { Item } = await dynamo.putItem({
    TableName: config.get('ORDER_TABLE'),
    Item: item,
  });

  return Item;
};

module.exports = { createOrder };
