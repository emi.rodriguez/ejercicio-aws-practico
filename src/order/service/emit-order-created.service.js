const sns = require('ebased/service/downstream/sns');
const config = require('ebased/util/config');

const emitOrderCreatedEvent = async (orderCreatedEvent) => {
  const { eventPayload, eventMeta } = orderCreatedEvent.get();

  await sns.publish(
    {
      TopicArn: config.get('ORDER_CREATED_TOPIC'),
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { emitOrderCreatedEvent };
