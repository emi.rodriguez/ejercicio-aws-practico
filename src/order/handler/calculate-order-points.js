const { batchEventMapper } = require('ebased/handler');
const inputMode = require('ebased/handler/input/batchEventQueue');
const outputMode = require('ebased/handler/output/batchEventConfirmation');

const {
  calculateOrderPointsDomain,
} = require('../domain/calculate-order-points.domain');

module.exports.handler = async (events, context) => {
  return await batchEventMapper(
    { events, context },
    inputMode,
    calculateOrderPointsDomain,
    outputMode
  );
};
